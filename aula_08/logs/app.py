
import logging 
import logging.config

logging.config.fileConfig('logging.ini')

logger = logging.getLogger(__name__)

#logging.basicConfig(
#    level=logging.DEBUG,
#    format= '%(asctime)s - %(levelname)s - %(name)s: %(message)s',
#    filename='exemplo.log'
#)



#0 - debug <- momento de desenvolvimento / fora do escopo - ^ tamanho do log / perfomance
#1 - info  <- não implica em erro -> evento na sua aplicação
#2 - warning <- eventos relevantes , não vai encontro ao funcionalmento normal da app
#3 - error   <- exception 
#4 - critical <- uncaught exception

while True:
    try:
        n1=int(input('n1: '))
        n2=int(input('n2: '))

        res = n1/n2

    except ZeroDivisionError:
        logger.warning(f'Tentativa de divisão por zero - n1:{n1} ; n2:{n2}') #<- info mais detalhadas 
        print('Não dividirás por zero') # <- comunicação para o cliente <> segurança da app.
    except ValueError:
        logger.warning(f'Entrada de dados incorreta - n1:{n1} ; n2{n2}')
    except Exception as e:
        logger.error('Error inesperado', exc_info=True)
    else:
        logger.info(f'Operação realizada com sucesso - n1:{n1} ; n2:{n2} ; res:{res}')
