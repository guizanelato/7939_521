
from flask import Blueprint, current_app, jsonify

blueprint = Blueprint("route", __name__)

@blueprint.route("/hello")
def hello():
    current_app.logger.info(f'FILENAME:{__name__} -  Estou na blueprint')
    current_app.logger.warning(f' {__name__} Estou na blueprint com cautela')

    return jsonify({
        'message': 'teste de logs com bluprint'
    })
