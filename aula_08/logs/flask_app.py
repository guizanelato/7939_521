
import logging.config

import flask

from blueprints.route import blueprint as hello

# logging
logging.config.fileConfig('logging.ini')

app = flask.Flask(__name__)
app.register_blueprint(hello)

@app.route("/", methods = ['GET'])
def index():
    app.logger.debug(" MENSAGEM DE DEBUG ")
    app.logger.info(" MENSAGEM DE INFO ")
    app.logger.warning(" MENSAGEM DE WARNING ")
    app.logger.error(" MENSAGEM DE ERROR ")
    app.logger.critical(" MENSAGEM DE CRITICAL")

    return flask.jsonify({'teste': 'uso de logs com flask'})


if __name__ == "__main__":
    app.run(host='0.0.0.0')


