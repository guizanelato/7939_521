from flask import Blueprint, render_template, request
import requests

blueprint = Blueprint("users", __name__)


@blueprint.route("/")
def get_users():

    # 1 Coletar a info:
    try:
        r = requests.get("http://localhost:5000/api/v1/users") #<- validação
        r.raise_for_status()
    except requests.exceptions.HTTPError as err:
        return r.status_code , {
            'ACK' : False,
            'mensagem' : "Não foi possível contatar a API",
            'error' : f'{err}'
        }

    except requests.exceptions.Timeout as err:
        return 408 , {
            'ACK' : False,
            'mensagem' : "Tempo limite excedido",
            'error' : f'{err}' # <- log! 
        }
    except request.exceptions.ConnectionError as err:
        return 500 , {
            'ACK' : False,
            'mensagem' : "Erro de conexão",
            'error' : f'{err}'
        }
    else:
        #2. Preparar o template/visualização do usuário final
        payload = r.json()
        return render_template("index.html", context=payload)



    ## status code 
    ## r.text
    ## r.json() -> dicionário do python

