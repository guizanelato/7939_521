
import flask
from api.get_users import blueprint as get_users
from api.delete_users import blueprint as delete_users
from api.add_users import blueprint as add_users
from api.update_users import blueprint as update_users

from views.users import blueprint as view_users

app = flask.Flask(__name__)

app.register_blueprint(get_users)
app.register_blueprint(delete_users)
app.register_blueprint(add_users)
app.register_blueprint(update_users)
app.register_blueprint(view_users)

## diretivas de configuração 
app.config['ENV'] = 'development'
app.config['DEBUG'] = True
app.config['JSON_SORT_KEYS'] = True
app.config['TEMPLATES_AUTO_RELOAD'] = True

## registro de rotas (blueprints)
## 
@app.errorhandler(404)
def error_404(err):
    return 'Página não encontrada!'


if __name__ == '__main__':
    app.run(host='0.0.0.0')
