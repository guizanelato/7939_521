from flask import Blueprint, jsonify, request
from extensions.montydb_utils import *


blueprint = Blueprint("update_users", __name__)


@blueprint.route('/users/update', methods = [ 'POST' ])
def add_user():
    db = get_database('user')
                        # query de filtro
    db.users.update_one({'username': request.json.get('username') }, {'$set': request.json})

    payload = [
        {
            'username' : u.get('username'),
            'password' : u.get('passwd')

        } for u in db.users.find({'username' : request.json.get('username') })
    ]

    # validação

    return jsonify({
        'ACK'  : 'Usuário atualizado com sucesso!',
        'user' : payload[0]
    })
