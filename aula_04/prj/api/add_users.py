from flask import Blueprint, jsonify, request
from extensions.montydb_utils import *


blueprint = Blueprint("add_users", __name__)


@blueprint.route('/users/add', methods = [ 'POST' ])
def add_user():
    db = get_database('user')

    db.users.insert_one(request.json)

    payload = [
        {
            'username' : u.get('username'),
            'password' : u.get('passwd')

        } for u in db.users.find({'username' : request.json.get('username') })
    ]

    # validação

    return jsonify({
        'ACK'  : 'Usuário adicionado com sucesso!',
        'user' : payload[0]
    })
