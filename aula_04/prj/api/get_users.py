
from flask import Blueprint, jsonify, render_template
from extensions.montydb_utils import *


                                                # localhost:5000/api/v1/endpoint
blueprint = Blueprint("get_users", __name__, url_prefix="/api/v1")

@blueprint.route('/users')
def get_users():
    db = get_database('user')

    payload = [{
        'username' : u.get('username'),
        'password' : u.get('passwd')

    } for u in db.users.find()]

    return jsonify(payload)

@blueprint.route('/users/<username>')
def get_user_by_username(username):
    db = get_database('user')

    payload = [
        {
            'username': u.get('username'),
            'password': u.get('passwd')
        } for u in db.users.find({'username': username})
    ]

    if len(payload) != 0:
        return jsonify(payload[0]) 
    
    return jsonify({'mensagem': 'usuário não encontrado'})



