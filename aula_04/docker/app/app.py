import flask


app = flask.Flask(__name__)

app.config['DEBUG'] = True
app.config['TEMPLATE_AUTO_RELOAD'] = True


@app.route("/")
def hello():
    return flask.jsonify({'Mensagem': 'Olá, estou dentro de um contêiner!'})


@app.route("/outra")
def outro_hello():
    return flask.jsonify({'Mensagem': 'Olá, estou dentro de um contêiner novamente!'})

@app.route("/users")
def get_users():
    return flask.jsonify({'Mensagem': 'desenvolvendo direto do contêiner, e modificando a rota'})



if __name__ == '__main__':
    app.run(host='0.0.0.0')
