#0 : imports
import flask

from api.auth import blueprint as auth
from api.profile import blueprint as profile

from views.login import blueprint as login

from dotenv import load_dotenv

app = flask.Flask(__name__)

load_dotenv()

#1: configs
app.config['TEMPLATE_AUTO_RELOAD'] = True
app.config['DEBUG'] = True


#2: blueprints
app.register_blueprint(auth)
app.register_blueprint(login)
app.register_blueprint(profile)

@app.route("/login")
def teste_login():
    return flask.render_template('login.html')


@app.route("/")
def teste_profile():
    return flask.render_template('index.html')

if __name__ == "__main__":
    app.run(host='0.0.0.0')
