
import requests

from os import getenv

from flask import ( Blueprint
                  , jsonify
                  , make_response
                  , redirect
                  , render_template
                  , request)

from extensions.jwt_utils import jwt_required


blueprint = Blueprint('login', __name__)


@blueprint.route("/login" , methods = ['GET', 'POST'])
def login():
    if request.method == 'POST':
        #lógica de processamento do form
        #0: coletar os dados do formulário

        payload = {
            'username': request.form.get('username'),
            'password': request.form.get('password')
        }

        #1: Verificar se as credenciais são válidas 
        #2: Enviar os dados de login para a api de autenticação
        
        resposta = requests.post(f'{getenv("API_URL")}/auth', json=payload)
        # { 'ACK' : Bool, token : 'aspisdogsjgkbsgjks'}

        refresh_token = resposta.json()
                        # true/false if [bool] == True
        if refresh_token['ACK']:
            # criar uma nova response e adicioanr o cookie com o token
            redir = make_response(redirect(f"{getenv('APP_URL')}/profile"))
            redir.set_cookie(key='token',
                    value = refresh_token.get('token'),
                    httponly = True)
            redir.set_cookie(key='user_id', 
                    value= payload.get('username'),
                    httponly=True)
            return redir
    return render_template('login.html')


@blueprint.route('/profile')
@jwt_required(token_type='refresh')
def get_profile():
    #1: pegar o token de acesso
    access_token = requests.get(f'{getenv("API_URL")}/auth/get_access_token',
            cookies=request.cookies)

    access_token = access_token.json()['token']
    #2: realizar a requisição para API protegida referente aos dados de profile
    r = requests.post(
            f'{getenv("API_URL")}/get_profile_data',
            headers = {'Authorization' : f'Bearer {access_token}'},
            json= {'user_id' : request.cookies.get('user_id')}
    )

    profile_data = r.json()['data']

    #return jsonify(profile_data)
    #return jsonify(r.json())
    return render_template("index.html" , context=profile_data)
