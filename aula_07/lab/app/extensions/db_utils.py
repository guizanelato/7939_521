
from os import getenv

import pymongo

MONGO_ADDR = getenv('MONGO_ADDR')

def get_database(database_name, mongo_addr=MONGO_ADDR):
    client = pymongo.MongoClient(host=mongo_addr)
    return client.get_database(database_name)


