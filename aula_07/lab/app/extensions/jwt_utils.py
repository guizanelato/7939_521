
import datetime

from os import getenv
from functools import wraps

from flask import jsonify, request

import jwt

#1: decorator de token
def jwt_required(token_type="access"):
    def token_required(fn):
        @wraps(fn)
        def wrapped(*args, **kwargs):
            token = None
            try :
                if token_type == 'access':     # Bearer fpsdijgsgigjs.wijtorg.osdifosd
                    token = request.headers.get('Authorization').split()[-1]
                else:
                    token = request.cookies.get('token') # key : value | token : 13132342

                jwt.decode(
                        token,
                        getenv('ACCESS_KEY') if 
                           token_type == 'access' else
                        getenv('REFRESH_KEY'),
                        algorithms = ['HS256']
                )
            except Exception as e: # granularizar os erros de acordo com jwt.exceptions
                return jsonify({
                    'ACK' : False,
                    'mensagem': 'Token Inválido',
                    'token' : token,
                    'error' : f'{e}'
                })
            return fn(*args, **kwargs)
        return wrapped
    return token_required


def generate_token(user_id, token_type="access"):
    return jwt.encode(
            {
                'user_id': user_id,
                'exp' : datetime.datetime.utcnow() +
                     datetime.timedelta(minutes = 5 if 
                         token_type=='access' else
                         100)
            },
            getenv('ACCESS_KEY') if 
                token_type == 'access' else
                getenv('REFRESH_KEY'),
            algorithm= 'HS256'
    )
