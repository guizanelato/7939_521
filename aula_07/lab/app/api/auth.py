
from flask import Blueprint, jsonify, request

from extensions.db_utils import get_database
from extensions.jwt_utils import generate_token, jwt_required

blueprint = Blueprint(
            'auth',
            __name__,
            url_prefix='/api/v1'
        )

@blueprint.route('/auth/get_access_token')
@jwt_required(token_type="refresh")
def get_access_token():
    
    user_id = request.cookies.get('user_id')

    return jsonify({
        'ACK': True,
        'token': generate_token(user_id=user_id, token_type="access")
    })


@blueprint.route('/auth', methods = ['POST'])
def auth():
    #1: coletar informações da requisição
    username = request.json.get('username')
    password = request.json.get('password')

    #2: acessar o banco de dados
    db = get_database('user')

    #3: consultar o dado de usuário a partir das informações  #criar validações exceptions
    user = db.users.find_one({'username' : username})

    #4: validar os dados
    credentialIsValid = username == user.get('username') and \
            password == user.get('password')

            
    if credentialIsValid:
        return jsonify({
            'ACK': True,
            'token' : generate_token(username, token_type='refresh')
        })

    return jsonify({
        'ACK': False,
        'mensagem': 'Usuário ou senha inválidos'
    })
