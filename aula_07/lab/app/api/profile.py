
from flask import Blueprint, jsonify, request

from extensions.db_utils import get_database
from extensions.jwt_utils import jwt_required

blueprint = Blueprint("profile",
        __name__,
        url_prefix="/api/v1")


@blueprint.route('/get_profile_data', methods = ['POST'])
@jwt_required(token_type="access")
def get_profile_data():
    db = get_database('user')

    user_id = request.json.get('user_id')

    profile_data = [
        {
            'full_name': u['profile']['full_name'],
            'position' : u['profile']['position'],
            'email'    : u['username'],
            'registration_date' : u['profile']['registration_date']

            } for u in db.users.find({'username': user_id})
    ]

    # validação se o usuário existe

    if profile_data:
        return jsonify({
            'ACK': True,
            'data': profile_data[0]
        })
    return jsonify({
        'ACK': False,
        'Mensagem': 'Usuário não encontrado'
    })



