
import datetime
from functools import wraps

import flask
import jwt
import requests


app = flask.Flask(__name__)
app.config['JWT_SECRET'] = 'Isso é um segredo'


def jwt_required(fn):
    @wraps(fn)
    def wrapped(*args, **kwargs):
        # da onde vem o token que o flask vai inspecionar?
        # request
        # Authorization : Bearer idfodj@#$23.23423khdfosdmfmwgwgsfv.234
        token = flask.request.headers.get('Authorization').split()[-1]
        try:
            jwt.decode(
                   token,
                   app.config['JWT_SECRET'],
                   algorithms = ['HS256'])

        except jwt.exceptions.ExpiredSignatureError:
            return flask.jsonify({
                'ACK': False,
                'mensagem': 'Token Expirado'
            })
        except jwt.exceptions.InvalidSignatureError:
            return flask.jsonify({
                'ACK': False,
                'mensagem': 'Assinatura inválida'
            })
        except jwt.exceptions.DecodeError:
            return flask.jsonify({
                'ACK': False,
                'mensagem': 'Token Invalido'
            })
        except Exception as e:
            return flask.jsonify({
                'ACK': False,
                'mensagem' : f'{e}'
            })
        return fn(*args, **kwargs)
    return wrapped




@app.route("/login", methods = [ 'GET' , 'POST' ] ) 
def login():
    if flask.request.method == 'POST':
        username = flask.request.form.get('username')
        password = flask.request.form.get('passwd')

        # parte de checagem de credenciais .... -> BD
        # username = fulano senha = 4linux

        credentialIsValid = username == 'fulano' and password == '4linux'

        if credentialIsValid:
            #1. criar um token
            #2. adicionar o tempo de expiração
            token = jwt.encode(
                {
                    'user_id' : username,
                    'exp': datetime.datetime.utcnow() + 
                            datetime.timedelta(minutes=1)
                },
                app.config['JWT_SECRET'],
                algorithm = 'HS256'
    
            )
            #3. criar uma nova requisiçÀo HTTP -> cabeçalho
            r =  requests.get("http://localhost:5000/protegida", 
                    headers= {'Authorization' : f'Bearer {token}' })

            return r.text

            # 'Authorization' : Bearer fiuaosf.323nkandifj,adfd
            #4. returna a reposta
            #return flask.redirect(flask.url_for('protegida'))


    return '''
        <h1> Login </h1>
        <form method="POST">
           <input type="text" name="username">
           <input type="password" name="passwd">
           <input type="submit">
        </form>

    '''


# verificar antes de entrar na rota, se a sessão está válida
@app.route("/protegida")
@jwt_required
def protegida():
    return "Área protegida por token"






