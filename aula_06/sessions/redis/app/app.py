# imports
from functools import wraps

import flask
import redis

from flask_session import Session

# definição da app
app = flask.Flask(__name__)

# configurações
app.secret_key = 'SDjfskdnsG#$@NODNF@#$' # <- variável de ambiente

app.config['FLASK_ENV'] = "development"
app.config['DEBUG'] = True
app.config['SESSION_TYPE'] = 'redis'
app.config['SESSION_PERMANENT'] = False    # redis://<ip>:<porta>
app.config['SESSION_REDIS'] = redis.from_url('redis://200.100.50.30:6379')

# session

server_session = Session(app)

def login_required(fn):
    @wraps(fn)
    def decorated_login_required(*args, **kwargs):
        if not flask.session.get('auth'): # se a sessão não estiver autenticada...
            return flask.redirect('/login')
        return fn(*args, **kwargs)
    return decorated_login_required


# login:
## 1- coletar as informações de credenciais (GET)
## 2- processar a lógica de validação das credenciais (POST)
@app.route("/login", methods = [ 'GET' , 'POST' ] ) 
def login():
    if flask.request.method == 'POST':
        username = flask.request.form.get('username')
        password = flask.request.form.get('passwd')

        # parte de checagem de credenciais .... -> BD
        # username = fulano senha = 4linux

        credentialIsValid = username == 'fulano' and password == '4linux'

        if credentialIsValid:
            flask.session['auth'] = True
            return flask.redirect(flask.url_for('protegida'))


    return '''
        <h1> Login </h1>
        <form method="POST">
           <input type="text" name="username">
           <input type="password" name="passwd">
           <input type="submit">
        </form>

    '''


# verificar antes de entrar na rota, se a sessão está válida
@app.route("/protegida")
@login_required
def protegida():
    return flask.render_template_string("""
        <h3> {{ session['auth']  }}

    """)


@app.route("/logout")
@login_required 
def logout():
    del flask.session['auth']
    return flask.redirect('/login')


if __name__ == "__main__":
    app.run(host="0.0.0.0")
