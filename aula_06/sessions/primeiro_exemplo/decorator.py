from functools import wraps



def primeiro(fn):
    def decora_primeiro(*args, **kwargs):
        print("VAAAAAAI", end=" ")
        return fn(*args, **kwargs)
    return decora_primeiro


def segundo(fn):
    @wraps(fn)
    def decora_segundo(*args, **kwargs):
        print("DA-LHE!", end=" ")
        return fn(*args, **kwargs)
    return decora_segundo


@primeiro # __name__ => decora_primeiro
def grito_de_teste():
    print("BUGRE!")


@segundo # __name__ => grito_do_brasil_de_pelotas
def grito_do_brasil_de_pelotas():
    print("XAVANTE!")
