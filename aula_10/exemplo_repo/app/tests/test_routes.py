
import unittest

from app import app as flask_app

class TestRoutes(unittest.TestCase):
    def setUp(self):
        app = flask_app.test_client()
        self.response = app.get("/")
        #.....

    def makeAssertions(self):
        payload = {'message': 'Estou no Gitlab CI, e também no Heroku :D'}
        self.assertEqual(200, self.response.status_code)
        self.assertEqual(payload, self.response.json)
        #.....

    def testRouteStatusCode(self):
        self.makeAssertions()
