# App apenas para teste, utilize esse arquivo para gerenciar configs e 
# registrar blueprints


import flask
import pymongo

from dotenv import load_dotenv

from os import getenv

load_dotenv()

app = flask.Flask(__name__)

MONGO_DATA = getenv('MONGO_DATA')
MONGO_URI= getenv('MONGO_URI')
MONGO_USER = getenv('MONGO_USER')
MONGO_PASS = getenv('MONGO_PASS')


@app.route("/")
def index():
    return flask.jsonify({"message": "Estou no Gitlab CI, e também no Heroku :D"})

@app.route("/mongo-atlas")
def get_mongo():
    client = pymongo.MongoClient(MONGO_URI.format(
        username=MONGO_USER,
        password=MONGO_PASS))

    db = client.get_database(MONGO_DATA)

    user = db.user.find_one({}, {'_id': 0})

    return flask.jsonify(user)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=getenv('PORT'))
