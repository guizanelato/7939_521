# Utilizando o Gitlab CI

## Sobre o repositório

Este repositório é dedicado a implementação de uma pipeline voltada a versionamento de artefatos de uma aplicação python. 

## Como utilizar

colocar uma descrição de como utilizar o projeto 

## Dependências

o que é preciso para que o projeto funcione

Exemplo de `dotenv`: 

```
PORT=5000
MONGO_PASS="insira a senha do usuario do mongo aqui"
MONGO_USER="usuario do mongo atlas "
MONGO_DATA="colocar o banco de dados"
MONGO_URI="mongodb+srv://{username}:{password}@cluster0.8z0o4.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"
```


## O que precisa ser feito

funcionalidades desejáveis ao projeto
