
import flask

from os import getenv

app = flask.Flask(__name__)

@app.route("/")
def index():
    return flask.jsonify({
        'message': 'Estou no Heroku!'
    })


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=getenv('PORT'))
