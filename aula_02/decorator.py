

"""VAAAAAI <NOME DO TIME>"""


def grito_de_torcida(fn):
    def funcao_decorada(*args, **kwargs):
        print('VAAAAIII', end=" ")
        fn(*args, **kwargs)
    return funcao_decorada

@grito_de_torcida
def grito_da_lusa():
    print("FABULOSA!")

@grito_de_torcida
def grito_do_sampaio_correia():
    print("BOLÍVIA QUERIDA DO MARANHÃO")
