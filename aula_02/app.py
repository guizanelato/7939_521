import flask
import montydb

montydb.set_storage('data')

app = flask.Flask(__name__)

#type annotations
def get_database(database):
    try:
        client = montydb.MontyClient('data')
        return client.get_database(database)
    except Exception as e:
        return 500, {
            'ACK' : False,
            'message': 'Não foi possível conectar ao banco de dados',
            'err': f'{e}'
        }

# viacep https://viacep.com/ 


        #endpoint         #verbos habilitados do prot. http
@app.route("/", methods = ['GET'])
def ex_rota():
    return flask.jsonify({
        'mensagem' : 'Hello from Flask!'
    })

@app.route("/outra-rota")
def outra_rota():
    return flask.jsonify({'mensagem': 'outra mensagem'})


@app.route("/users", methods= ['GET'])
def get_users():
    #1: iniciar uma conexão com o banco
    db = get_database('user')
    #2: realizar a consulta dos usuários
    payload = [
        {
            'username' : u.get('username'),
            'password' : u.get('passwd')

        } for u in db.users.find()
    ]
    #3: retornar no formato de json os usuários encontrados
    return flask.jsonify(payload)

@app.route('/users/<username>')
def get_user_by_username(username):
    db = get_database('user')

    payload = [
        {
            'username' : u.get('username'),
            'password' : u.get('passwd')

            } for u in db.users.find({'username': username})
    ]

    return flask.jsonify(payload[0])


@app.route('/users/add', methods = ['POST'])
def add_user():
    db = get_database('user')

    db.users.insert_one(flask.request.json)

    return flask.jsonify({
        'ACK' : 'usuário inserido com sucesso!',
        'data': { 'username': flask.request.json.get('username'),
                  'password': flask.request.json.get('passwd')} 
    })


@app.route('/users/update/<username>', methods=['PUT'])
def update_user(username):
    db = get_database('user')

    db.users.update_one({'username': username}, {'$set' : flask.request.json})

    payload = [
        {
          'username': u.get('username'),
          'password'  : u.get('passwd')
        } for u in db.users.find({'username': username})
    ]

    return flask.jsonify(payload[0] if payload is not None else {'message' : 'usuário não encontrado'})


@app.route('/users/delete/<username>', methods = ['DELETE'])
def delete_by_username(username):
    db = get_database('user')

    db.users.delete_one({'username' : username })
    ### validação 

    return flask.jsonify({'message' : 'usuário removido com sucesso!'})





# flask como um script de python    
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
