
import flask
from routes.get_users import blueprint as get_users
from routes.delete_users import blueprint as delete_users

app = flask.Flask(__name__)

app.register_blueprint(get_users)
app.register_blueprint(delete_users)

## diretivas de configuração 
## registro de rotas (blueprints)
## 

#if __name__ == '__main__':
#    app.run(host='0.0.0.0', debug=True)
