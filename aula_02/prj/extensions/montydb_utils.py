import montydb

STORAGE_NAME = 'data'

def set_storage(storage_name):
    montydb.set_storage(storage_name)

def get_database(database_name):
    set_storage(STORAGE_NAME)
    client = montydb.MontyClient(STORAGE_NAME)
    return client.get_database(database_name)


