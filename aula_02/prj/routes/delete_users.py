
from flask import Blueprint, jsonify
from extensions.montydb_utils import *

blueprint = Blueprint("delete_users", __name__)

@blueprint.route('/users/delete/<username>', methods = [ 'DELETE' ])
def delete_user_by_username(username):
    db = get_database('user')

    db.users.delete_one({'username': username})

    return jsonify({'mensagem': 'usuário removido com sucesso!'})

