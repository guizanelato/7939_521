
from flask import Blueprint, jsonify
from extensions.montydb_utils import *

blueprint = Blueprint("get_users", __name__)

@blueprint.route('/users')
def get_users():
    db = get_database('user')

    payload = [{
        'username' : u.get('username'),
        'password' : u.get('passwd')

    } for u in db.users.find()]

    return jsonify(payload)
