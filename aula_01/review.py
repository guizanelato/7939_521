# Review de Python

## json <> python: dicionários

payload = {
  'nome' :  'ada lovelace',
  'email':  'ada.lovelace@4linux.com.br'
}

## acesso a dados em dicionário

print(payload['nome']) # ada lovelace
print(payload.get('nome')) # ada lovelace

## filtrar uma informação email do dicionário

## laço, loops
              # [nome, email]
for chave in payload.keys(): # para cada chave do meu dicionário
    if chave == 'email': # caso for e-mail
        print(f'achei: {payload.get(chave)}') # f-string

## tuplas e listas 
              # 0                      1
tupla = ('ada lovelace', 'ada.lovelace@4linux.com.br')
tupla[0]
tupla[-1] # retorna o último elemento

lista = ['ada lovelace' , 'ada.lovelace@4linux.com.br']


##  Comprehensions 


resultado = [
  {
      'A': u*1 ,
      'B': u*2,
      'C': u*3
  } for u in range(10)
]


## classes e funções 


               # call operator
               #   v
def nome_da_funcao(num1, num2):
    ## passagem por valor -> dados imutáveis : strings, inteiros, floats, tuplas 
    ## passagem por referência dados mutáveis: dicionários, listas 
    # bloco de código a ser executado
    return num1 + num2


print(nome_da_funcao(10,2))
print(nome_da_funcao(22,15))
print(nome_da_funcao(123,44))
print(nome_da_funcao(22,33))

variavel_qualquer = lambda x,y: x + y

variavel_qualquer(10,2)



class Pessoa:
    def __init__(self
            , nome= 'ada'
            , sobrenome = 'lovelace'
            , idade = 35

    ): # construtor
        ## atributos <> variáveis
        self.nome = nome
        self.sobrenome = sobrenome
        self.idade = idade

    def apresentacao(self): # métodos <> funções
        return f'Olá meu nome é {self.nome} {self.sobrenome} ' \
               f'Tenho {self.idade} anos'


p1 = Pessoa() # assumir os valores padrão
p2 = Pessoa(nome='José', sobrenome='almeida', idade=45)


## testes exceções 

try: # indicar um bloco que pode dar problema
    num1 = float(input('n1: '))
    num2 = float(input('n2: '))
    print(num1/num2)

except ZeroDivisionError:
    print("não dividirás por zero")
except ValueError:
    print('Informe apenas números')
except Exception as e:
    print(f'Erro inesperado - {e}')

else: # em caso de sucesso
    print('foi bem sucedido')

finally:
    print('sempre executarei esse trecho de código')



