
# Python for API

## Aula 01

### Onboard 
### Infraestrutura do curso -> Python 3.6+ / Docker | VM Debian 10

### Conteúdo do Curso

#### dia 1: review python + requests (como enviar/consumir endpoints) | 
     Contexto Web | Protocolo HTTP (quem consome)

#### dia 2/3: flask  (quem serve) 
            comandos 
            instala
            funcionamento interno
            como criar endpoints e apis
            templates -> Jinja2 
                         macros, modulos
            blueprint 
            projetos 
            api integra com o mongdb   

#### dia 3/4: contêiner 
            Docker
            conteiner x vms
            Docker CLI
            executar contêineres
            criar imagens
            criar volumes
            criar redes
            uso de variáveis de ambiente
            docker-compose 
            conteinerizar a aplicação


#### dia 4/5 : Autenticação e autorização
             Cookie
             Session
             Flask <> Sessions
             Stateful x Stateless
             Tokens JWT 
             Implementa autenticação / autorização


#### dia 5  :  Logs
            modulo logging
            criar arquivo de configuração de log
            json

#### dia 6 : Projeto/ Serviços
           # Ambiente de Desenvolvimento Moderno
             > Versionamento de Código <> Git + GitLab-CI (runners)  
             > Integração contínua ( automação ) Pipelines Testes  
             > Versionamento de artefato > Imagem de Contêiner (Registry) Dockerhub 
             > Deploy -> homolog/staging > Heroku (Dyno)
               
#### dia 7: Versionamento código com git + gitlab
          > o que é o git / como funciona / sua estrutura de dados
          > principais usos 
            >> Modelos de projeto (GitFlow | Trunk Based) 
          > gitlab como gerenciador de repositórios do git
           >> Issue Oriented Flow 

#### dia 8: CI com Gitlab
         > Construir pipelines
         > como automatizar testes
         > como integrar o gitlab com dockerhub
         > Gerenciamento de credenciais e usos de variável de ambiente
         > prévia desse versionamento de artefatos


#### dia 9: Heroku
          CLI do heroku
          publicação de uma app no heroku
          uso de imagens de contêiner no heroku
          integrar o heroku na pipeline do GitLab 
 
#### dia 10: Projeto <Desafio> 
          simular um teste técnico
          focado em dev backend 
          api que consome um banco de dados
          + utilizarem tecnologias de contêiner
          + demonstrarem conhecimento de etapas de CI 

