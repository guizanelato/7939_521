# Exercício 1: Consumir uma lista de ceps na API ViaCEP

## Objetivo: coletar os dados de cep e estruturar em um arquivo .csv

### cabec1,cabec2,
### info1;info2;info3;\n

import requests

ceps = ['06223040', '04101300', '80530230', '22241200']

url = 'https://viacep.com.br/ws/{cep}/json/'

cabec = 'cep;logradouro;complemento;bairro;localidade;uf;ibge;gia;ddd;siafi\n'

dados = []
dados.append(cabec)

for cep in ceps:
    r = requests.get(url.format(cep=cep))
    if r.status_code == 200:
        payload = r.json()
        dados.append(f'{payload["cep"]};' \
                     f'{payload["logradouro"]};' \
                     f'{payload["complemento"]};' \
                     f'{payload["bairro"]};' \
                     f'{payload["localidade"]};' \
                     f'{payload["uf"]};' \
                     f'{payload["ibge"]};' \
                     f'{payload["gia"]};' \
                     f'{payload["ddd"]};' \
                     f'{payload["siafi"]}\n')





with open('dados_cep.csv', 'x') as arq:
    arq.writelines(dados)

