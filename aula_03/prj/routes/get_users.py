
from flask import Blueprint, jsonify, render_template
from extensions.montydb_utils import *

blueprint = Blueprint("get_users", __name__)

@blueprint.route('/users')
def get_users():
    db = get_database('user')

    payload = [{
        'username' : u.get('username'),
        'password' : u.get('passwd')

    } for u in db.users.find()]

    return jsonify(payload)

@blueprint.route('/users/<username>')
def get_user_by_username(username):
    db = get_database('user')

    payload = [
        {
            'username': u.get('username'),
            'password': u.get('passwd')
        } for u in db.users.find({'username': username})
    ]

    if len(payload) != 0:
        return jsonify(payload[0]) 
    
    return jsonify({'mensagem': 'usuário não encontrado'})

@blueprint.route('/users/template') # front + backend
def get_users_template():
    db = get_database('user')

    payload = [{
        'username' : u.get('username'),
        'password' : u.get('passwd')

    } for u in db.users.find()]

    return render_template("index2.html", context=payload)
