import flask
import pymongo

from dotenv import load_dotenv

from os import getenv

load_dotenv()


MONGO_ADDR = getenv('MONGO_ADDR')

client = pymongo.MongoClient(host=MONGO_ADDR)

db = client.get_database('user')

app = flask.Flask(__name__)

app.config['DEBUG'] = True
app.config['TEMPLATE_AUTO_RELOAD'] = True


@app.route("/")
def hello():
    return flask.jsonify({'Mensagem': 'Olá, estou dentro de um contêiner!'})


@app.route("/outra")
def outro_hello():
    return flask.jsonify({'Mensagem': 'Olá, estou dentro de um contêiner novamente!'})

@app.route("/users")
def get_users():

    payload = [
      {
          'username': u.get('username'),
          'password': u.get('senha')

      } for u in db.users.find()
    ]

    return flask.jsonify(payload)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
